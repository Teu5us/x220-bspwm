#
# wm independent hotkeys
#

super + x
	emacsclient -c -e '(org-capture)'

super + shift + z
	emacsclient -c -e '(org-agenda-list)'

# function keys
# make sxhkd reload its configuration files:
super + F1
	pkill -USR1 -x sxhkd
# quit/restart bspwm
super + F2
	bspc wm -r
super + F3
	displayselect
# Hibernate
super + F4
	prompt 'Hibernate computer?' 'sudo s2ram'
# Restart/rescan wifi/eth networks
super + F5
	sudo -A rc-service NetworkManager restart
# Start torrent daemon/open interface
super + F6
	torwrap
# toggle torrent daemon
super + F7
	td-toggle
# sync email
super + F8
	mailsync
# Mount a USB drive or Android device
super + F9
	dmenumount
# Unmount a USB drive or Android device
super + F10
	dmenuumount
# Search with Duckduckgo
super + F11
	ducksearch
# Network Manager interface
super + F12
	$TERMINAL -e sudo -A nmtui

# Recording
# Take screenshot
Print
	scrot pic-full-$(date '+%y%m%d-%H%M-%S').png
# Pick screenshot type
shift + Print
	scrotpick
# Record audio or video
super + Print
	dmenurecord
# kill active recording
super + Delete
	dmenurecord kill

# volume
super + {minus,equal}
	amixer -q sset Master 5%{-,+}

XF86AudioLowerVolume
	amixer -q sset Master 5%-

XF86AudioRaiseVolume
	amixer -q sset Master 5%+

# mute
XF86AudioMute
	amixer -q sset Master toggle

# telegram
super + e
	telegram-desktop
# emacs
super + z
	ecl

# nvim term
super + y
	$TERMINAL -e nvim

# htop
super + i
	$TERMINAL -e htop

# clipmenu
super + c
	clipmenu

# file browser
super + r
	$TERMINAL -e vu

# file browser
super + shift + r
	xfe

# layout switcher
super + a
	xkb-switch -n

# file opener
super + alt + a
	$TERMINAL -e open

# terminal emulator
super + Return
	$TERMINAL

# # scratchpad
# super + u
# 	id=$(cat /tmp/scratchid);\
# 	bspc node $id --flag hidden;bspc node -f $id

# browsers
super + w
	firefox

# program launcher
super + d
	dmenu_run

#
# power
#

super + shift + x
	prompt "Shutdown?" "sudo poweroff"

super + alt + x
	prompt "Reboot?" "sudo reboot"


#
# bspwm hotkeys
#

# quit bspwm
super + Escape
	prompt "Quit?" "bspc quit"

# close and kill
super + {_,shift + }q
	bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# # send the newest marked node to the newest preselected node
# super + y
# 	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
super + control + b
	bspc node -s biggest

#
# state/flags
#

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,ctrl + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
# super + {p,b,comma,period}
# 	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
# super + {_,shift + }c
# 	bspc node -f {next,prev}.local

# focus prev/next desktop
super + {comma,period}
	bspc monitor -f {next,prev}

# send window to prev/next desktop
super + ctrl + {comma,period}
	bspc node -m {next,prev}

# focus the next/previous desktop in the current monitor
super + {g,semicolon}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
	bspc {desktop,node} -f last

# focus the older or newer node in the focus history
super + alt + {comma,period}
	bspc wm -h off; \
	bspc node -f {newer,older}.local; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,ctrl + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + shift + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + alt + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
# super + ctrl + space
super + alt + n
	bspc node -p cancel

# cancel the preselection for the focused desktop
# super + ctrl + shift + space
# 	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + {y,u,i,o}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
# super + {Left,Down,Up,Right}
super + control + {y,u,i,o}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# # hide window
# super + v
#   bspc node -g hidden

# # unhide window
# super + shift + v
#   bspc node {,$(bspc query -N -n .hidden | tail -n1)} -g hidden=off

# gaps switch (dynamic/no gaps)
super + alt + g
	echo 0 > $HOME/.wgaps && wgaps
super + ctrl + g
	echo 50 > $HOME/.wgaps && wgaps
