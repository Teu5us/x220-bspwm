#!/bin/bash

arg=$1 # Get the argument
winid=$(xdotool getactivewindow) # Pick a window
change=25 # Size change in pixels
wingap=6 # Gap between window and screen edges
mainmonW=1366 # Primary monitor width
GW=$(xwininfo -id $winid | awk '/Width/ {print $2}') # Chosen window width
GH=$(xwininfo -id $winid | awk '/Height/ {print $2}') # Chosen window height

#------------------------------------------------------------------------------
# What's required for resizing
#------------------------------------------------------------------------------

# Set new size
resize() {
	xdotool windowfocus $winid windowsize $winid $1 $2
}

# Increase height downwards
rdown() {
	(( H=$GH+$change ))
	resize $GW $H
}

# Increase width to the right
rright() {
	(( W=$GW+$change ))
	resize $W $GH
}

# Decrease height upwards
rup() {
	(( H=$GH-$change ))
	resize $GW $H
}

# Decrease width to the left
rleft() {
	(( W=$GW-$change ))
	resize $W $GH
}

#------------------------------------------------------------------------------
# Move window into the screen if it overlaps its border
#------------------------------------------------------------------------------

monW=$(herbstclient monitor_rect | awk '{print $3}') # Monitor width
monH=$(herbstclient monitor_rect | awk '{print $4}') # Monitor height

# Required with a second monitor
if [ $monW -gt $mainmonW ]; then
	(( mW=$monW-$mainmonW ))
else
	mW=$monW
fi

# Window pos
PX=$(xwininfo -id $winid | awk '/Absolute upper-left X/ {print $4}')
PY=$(xwininfo -id $winid | awk '/Absolute upper-left Y/ {print $4}')

# Count new window position
move() {
	(( checkW = $PX + $GW ))
	(( checkH = $PY + $GH ))
	if [ $checkW -gt $mW ]; then
		(( difW=$checkW-$mW ))
		(( NPX=$PX-$difW-$wingap ))
		xdotool windowmove $winid $NPX $PY
	fi

	if [ $checkH -gt $monH ]; then
		(( difH=$checkH-$monH ))
		(( NPY=$PY-$difH-$wingap ))
		xdotool windowmove $winid $PX $NPY
	fi
}

#------------------------------------------------------------------------------
# Dmenu prompt
#------------------------------------------------------------------------------

prompt() {
	pgrep -x dmenu && exit
	size=$(echo "Width Height" | dmenu -i -p "Resize:") || exit 1
	w=$(echo "$size" | cut -f1)
	h=$(echo "$size" | cut -f2)
	resize $w $h
}

#------------------------------------------------------------------------------
# Do the resizing
#------------------------------------------------------------------------------

case $arg in
	-d) rdown ;;
	-u) rup ;;
	-r) rright ;;
	-l) rleft ;;
	*) prompt ;;
esac

move
