HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000000
SAVEHIST=10000000
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing nonexistent history.

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
HIST_STAMPS=""

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Plugins {{{
if [ ! -d ~/.antigen ]; then
	mkdir ~/.antigen
	curl -L git.io/antigen > ~/.antigen/antigen.zsh
	source ~/.antigen/antigen.zsh
else
	if [ ! -f ~/.antigen/antigen.zsh ]; then
		curl -L git.io/antigen > ~/.antigen/antigen.zsh
		source ~/.antigen/antigen.zsh
	else
		source ~/.antigen/antigen.zsh
	fi
fi

antigen bundle mreinhardt/sfz-prompt.zsh

antigen use oh-my-zsh
antigen bundle git
antigen bundle srijanshetty/zsh-pandoc-completion
antigen bundle zdharma/fast-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

antigen apply

# source /home/paul/gits/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_STRATEGY="match_prev_cmd"
# source /home/paul/gits/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
# }}}

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc" # Load shortcut aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

# ssh set TERM to xterm first
alias ssh="TERM='xterm-256color' ssh"
alias nr="nvim -u ~/.config/nvim-ru/init.vim"

# terminal rickroll!
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# dotfile git alias
# alias config='/usr/bin/git --git-dir=/home/dt/dotfiles --work-tree=/home/dt'

# passwords
ac(){
	vim --clean -c 'set viminfo=' \
		-c 'set mouse=' \
		-c 'set clipboard=unnamedplus' \
		-c 'nmap ,c "+yiW' \
		$HOME/passwords
	r4=$(xclip -o -selection clipboard | sed "s/.*/\'&\'/")
	# sleep 10
	clipdel "$(xclip -o -selection clipboard)"
	clipdel $r4
}

alias cp="cp -i"                          # confirm before overwriting something
# alias df='df -h'                          # human-readable sizes

# System Maintainence
alias sdn="sudo -E shutdown now"
alias psref="gpg-connect-agent RELOADAGENT /bye" # Refresh gpg

# tmux
alias tn="tmux new-session -s"
alias ta="tmux a -t"
alias tl="tmux list-sessions"
alias clc="clipdel -d '.*'"

# Some aliases
alias e="$EDITOR"
alias p="sudo -E pacman"
alias SS="sudo -E systemctl"
alias v="nvim"
alias vt="nvim +Neomux"
alias nv="nvr -s"
alias sv="sudo -E nvim"
alias em="emacsclient -nw"
alias sem="sudo -E emacsclient -nw"
alias r="ranger"
alias sr="sudo -E ranger"
# alias rr="st -e ranger"
alias ka="killall"
alias g="git"
alias trem="transmission-remote"
alias mkd="mkdir -pv"
alias ref="shortcuts && source ~/.zshrc" # Refresh shortcuts manually and reload bashrc

# Adding color
# alias ls='ls -hN --color=auto --group-directories-first'
alias grep="grep --color=auto"
alias diff="diff --color=auto"
alias ccat="highlight --out-format=ansi" # Color cat - print file with syntax highlighting.

# Internet
alias yt="youtube-dl --add-metadata -ic" # Download video link
alias yta="yt -x -f bestaudio/best" # Download only audio
alias YT="youtube-viewer"

# TeX
alias Txa="cp ~/Documents/LaTeX/article.tex"
alias Txs="cp ~/Documents/LaTeX/beamer.tex"
alias Txh="cp ~/Documents/LaTeX/handout.tex"
alias Rmd="cp ~/Documents/LaTeX/rmarkdown.rmd"
alias Rios="cp ~/Documents/LaTeX/rioslides.rmd"
alias Elm="cp ~/Documents/code/elm/skeleton.elm"

####

zstyle ':completion:*' completer _expand_alias _complete _ignored
alias tb="nc termbin.com 9999"

bindkey -v
# bindkey '^R' history-incremental-search-backward
bindkey '^F' autosuggest-accept

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
FZF_TMUX=1

# export MANPAGER="/bin/sh -c \"col -b | nvim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""
export MANPAGER="/bin/sh -c \"col -b | nvim -c 'set ft=man ts=8 nomod nolist noma' -\""
