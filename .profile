#!/bin/sh
# Profile file. Runs on login.

# Adds `~/.local/bin` and all subdirectories to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//'):$HOME/.fzf/bin/:$HOME/node_modules/.bin/:$HOME/dotnet"
export DOTNET_ROOT=$HOME/dotnet
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export FILE="vu"
export BIB="$HOME/Documents/LaTeX/uni.bib"
export REFER="$HOME/Documents/referbib"
export SUDO_ASKPASS="$HOME/.local/bin/tools/dmenupass"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export FZF_DEFAULT_COMMAND="ag --hidden -g ''"
export FZF_DEFAULT_OPTS="-e --no-clear -1 -m --bind 'ctrl-r:execute(rm {})'"

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

# bspwm
export XDG_CONFIG_HOME="$HOME/.config"
# export TERM="xterm-256color"

# mpd >/dev/null 2>&1 &

[ ! -f ~/.config/shortcutrc ] && shortcuts >/dev/null 2>&1

echo "$0" | grep "bash$" >/dev/null && [ -f ~/.bashrc ] && source "$HOME/.bashrc"

choose_session() {
	echo -n "Choose session: hlwm or bspwm"
	read -r session
	[ -z $session ] && ch="herbstluftwm" || ch="bspwm"
}

# Start graphical server if bspwm not already running.
# [ "$(tty)" = "/dev/tty1" ] && ! pgrep -x bspwm >/dev/null && exec startx ~/.xinitrc bspwm -- vt1 >/dev/null 2>&1
# [ "$(tty)" = "/dev/tty2" ] && ! pgrep -x bspwm >/dev/null && exec startx ~/.xinitrc hlwm -- vt2 >/dev/null 2>&1

[ "$(tty)" = "/dev/tty1" ] && choose_session && \
	! pgrep -x $ch >/dev/null && exec startx ~/.xinitrc $session -- vt1 >/dev/null 2>&1

# Switch escape and caps if tty:
sudo -n loadkeys ~/.local/bin/ttymaps.kmap 2>/dev/null
