#!/bin/bash

## This script translates words and collocations using multitran.com.

### cURL #####################################################################
##############################################################################
curlen(){
	curl "https://www.multitran.com/m.exe?l1=2&l2=1&s=$(uri)" 2>/dev/null\
		| curlformat
}
##############################################################################

##############################################################################
curlde(){
	curl "https://www.multitran.com/m.exe?s=$(uri)&l1=2&l2=3" 2>/dev/null\
		| curlformat
}
##############################################################################

### Pandoc ###################################################################
##############################################################################
panden() {
	curl "https://www.multitran.com/m.exe?l1=2&l2=1&s=$(uri)" 2>/dev/null\
		| pandocformat
}
##############################################################################

##############################################################################
pandde() {
	curl "https://www.multitran.com/m.exe?s=$(uri)&l1=2&l2=3" 2>/dev/null\
		| pandocformat
}
##############################################################################

##############################################################################
curlformat(){
	sed -n "/<table\swidth.*/,/<table\scell.*/{
	s/<[^>]*>//g
	s/^\&nbsp\;/>>> /g
	s/\&nbsp\;/ /g
	s/^\w*\./--> &/g
	s/^-->.*\./${esc}[34m&${esc}[0m/
	s/^>>>/${esc}[31m${esc}[1m&${esc}[0m/
	p}" |\
		sed '1d; $d'
}
##############################################################################

##############################################################################
pandocformat(){
	sed -n "/<table\swidth.*/,/<table\scell.*/{
		s/\&nbsp\;/]==>/
		s/^.*subj.*>\(\w*\.\)/--> \1/
		p}" |\
	pandoc --wrap=none -f html - -t plain |\
	sed "s/^]==>/>>> /
			s/]==>//g
			s/^>>>/${esc}[31m${esc}[1m&${esc}[0m/
			s/^-->.*\./${esc}[34m&${esc}[0m/
			/^$/d
			1 i\

			\$a\ "
}
##############################################################################

##############################################################################
lang(){
	echo "Choose language pair:"
	echo "1) [en-ru] 2) [de-ru] 3) pandoc-[en-ru] 4) pandoc-[de-ru]"
	echo -n "> Choice: "
	read -r lang
	case "$lang" in
		1) lang=e ;;
		2) lang=g ;;
		3) lang=pe ;;
		4) lang=pg ;;
		"") lang ;;
		*) lang ;;
	esac
	[ "$l" != "-l" ] && interactive || interactive "$l"
}
##############################################################################

##############################################################################
interactive(){
	search(){
		main "$lang"
	}
	showsearch(){
		search="$(search)"
		[ -n "$search" ] && echo "$search"
	}
	# while :;
	for (( ; ; ))
	do
		echo -n "> Search: "
		read -r searchpattern
		reset
		case "$searchpattern" in
			':l') lang ;;
			':q') exit 1 ;;
			'') interactive "$l"
		esac
		if [ "$1" != "-l" ]; then
			showsearch
		else
			showsearch | less
		fi
	done
}
##############################################################################

##############################################################################
## Convert searchpattern to URL. Uncomment one, comment out the other one.

## Convert searchpattern to URL using perl.
# uri(){
# 	perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "${searchpattern}"
# }

## Convert searchpattern to URL using cURL, no extra stuff.
uri(){
	echo "${searchpattern}" | curl -Gso /dev/null -w %{url_effective}\
		--data-urlencode @- "" | sed -E 's/..(.*).../\1/'
}
##############################################################################

##############################################################################
## Choose interactive mode, cURL-only or pandoc (should be cleaner).
main(){
	checkpandoc(){
		[ -z "$(command -v pandoc)" ]\
			&& echo "Pandoc is not installed."\
			&& echo "Install pandoc or use 'e'."\
			&& echo "Not using 'p' this time."
	}
	case "$1" in
		e) curlen ;;
		g) curlde ;;
		pe) checkpandoc && curlen || panden ;;
		pg) checkpandoc && curlde || pandde ;;
		i) lang ;;
	esac
}
##############################################################################

##############################################################################
## Set some variables.
esc=$(printf '\033')
searchpattern=${*:2}
l="$2"

## '-l' can be used to pipe the output to `less`
if [ "$l" != "-l" ]; then
	main "$1"
else
	searchpattern=$(echo "$searchpattern" | sed "s/^-l\s//")
	[ "$1" = "i" ] && lang "$l" || main "$1" | less
fi
##############################################################################
